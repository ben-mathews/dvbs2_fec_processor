# DVB-S2 FEC Processor

Wrapper around AFF3CT library to perform DVB-S2 FEC processing

This repository contains code that wraps the AFF3CT library to perform FEC decoding for DVB-S2 signals.  Starting with LDPC processing.  Will add BCH decoding later.

## Building the Project

### Checking out the source
See [build dependencies](#Build Dependencies for Ubuntu 18.04) for pre-requsites.

Checkout the source code:

    git clone https://gitlab.com/ben-mathews/dvbs2_fec_processor.git

Get the AFF3CT library submodule:

	cd dvbs2_fec_processor
	git submodule update --init --recursive

### Compiling on Linux

This section assumes Ubuntu 20.04.  See notes about compiling on Ubuntu 18.04 below.  Other OS's should follow a similar procedure.

If necessary, first install some prerequisites:

    sudo apt-get install git build-essential cmake

To compile the library on Linux (probably same for MacOS/MinGW, but I have not tested these):

	mkdir build
	pushd build
	cmake ../ -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-funroll-loops -march=native" -DCMAKE_EXPORT_COMPILE_COMMANDS=1
	make -j18
	popd

Until I figure out how to massage CMake to do it, manually move library files into the Python wrapper folder:

    rm -f ./python/py_dvbs2_fec/py_dvbs2_fec/lib/*.so
    cp ./build/lib/aff3ct/lib/libaff3ct-*.so ./python/py_dvbs2_fec/py_dvbs2_fec/lib/
    cp ./build/src/dvbs2-fec-processor/lib/libdvbs2-fec-processor.so ./python/py_dvbs2_fec/py_dvbs2_fec/lib/

### Compiling on Windows
If necessary, first install some prerequisites:

1) Visual Studio 2017 or 2019 ([https://visualstudio.microsoft.com/downloads](https://visualstudio.microsoft.com/downloads)).  In the installer select the "Desktop development with C++" Workload.  Building from the command line may require that the `C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat` (for VS 2017) or `C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat` (for VS 2019) batch file be run to set certain environment variables in your DOS shell.  If after doing this the `devenv.exe` executable still can't be found, resolve it by running `set PATH=%PATH%;C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE` (use the appropriate path based on where you have `devenv` installed.
2) CMake ([https://cmake.org/download/](https://cmake.org/download/)), version >= 3.2.  I select the "Add Cmake to system PATH for all users" option.
3) Python (https://www.python.org/downloads/release/) if installing and using the Python bindings.  Get the x64 version.  Not sure if it's required, but when installing I select the "Install for all users" and "Add Python to environment variables" options, in addition to the default options.
3) Git ([https://git-scm.com/download/win](https://git-scm.com/download/win)) to download source code.

With the prerequisites installed, to build DVB-S2 FEC Processor on Windows, run:

    :: If necessart: C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvars64.bat
    :: If necessary: set PATH=%PATH%;C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\IDE
	mkdir build
	pushd build
	cmake .. -G"Visual Studio 16 2019" -DCMAKE_CXX_FLAGS="-D_CRT_SECURE_NO_DEPRECATE /EHsc /MP4 /bigobj" -DAFF3CT_COMPILE_EXE="OFF" -DAFF3CT_COMPILE_STATIC_LIB="ON" -DAFF3CT_COMPILE_SHARED_LIB="ON"
    :: or for VS 2017, run: cmake .. -G"Visual Studio 15 2017 Win64" -DCMAKE_CXX_FLAGS="-D_CRT_SECURE_NO_DEPRECATE /EHsc /MP4 /bigobj" -DAFF3CT_COMPILE_EXE="OFF" -DAFF3CT_COMPILE_STATIC_LIB="ON" -DAFF3CT_COMPILE_SHARED_LIB="ON"
	devenv /build Debug DVBS2-FEC-Processor.sln
	popd

Until I figure out how to massage CMake to do it, manually move library files into the Python wrapper folder:

    del /s python\py_dvbs2_fec\py_dvbs2_fec\*.dll  >nul 2>&1
    copy build\lib\aff3ct\lib\Debug\*.dll python\py_dvbs2_fec\py_dvbs2_fec\lib
    copy build\src\dvbs2-fec-processor\lib\Debug\dvbs2-fec-processor.dll python\py_dvbs2_fec\py_dvbs2_fec\lib


## Using the Python Wrapper

The Python wrapper in `python/py_dvbs2_fec` wraps the compiled C/C++ library and allows for FEC processing from within a Python environment.  The Python wrapper instantiates a bank of FEC processors, one for each possible code rate.  This is done because instantiating a FEC processor takes a non-trivial amount of time, so by instantiating a processor for all possible FEC types when the Python object is created, we can incur this hit only once.

To install the Python wrapper, cd into the `dvbs2_fec_processor/python/py_dvbs2_fec` directory and perform execute 

    pip3 install .  

In the example below we create a series of random Log-Likelihood Ratio (LLR) values which will cause the array to fail FEC processing, but this example demonstrates how to use this wrapper.  Use sane LLR values (e.g. generate them in Matlab or elsewhere) if you want to see this actually pass.

```Python
import numpy as np
from py_dvbs2_fec import DVBS2_FEC_Processor_Bank

# QPSK Rate 3/5 using short frames
frame_length = 'Short'
k_bch = 9552
k_ldpc = 9720
t_bch = 12
n_ldpc = 16200
q = 18

llr = np.random.randn(n_ldpc)

dvbs2_fec_processor_bank = DVBS2_FEC_Processor_Bank(frame_length=frame_length)

passed_ldpc_fec, bits = \
            dvbs2_fec_processor_bank.decode_ldpc(llr=llr, include_cw=True, n_ldpc=n_ldpc,
                                                 k_ldpc=k_ldpc, k_bch=k_bch, t_bch=t_bch)
# Expect this to fail, but at least run
```


## Build Dependencies for Ubuntu 18.04

Install some basic dependencies (libssl-dev will be needed to build cmake later):

    sudo apt-get install build-essential git libssl-dev python-pip

Ubuntu 18.04 uses a version of cmake that is too old for AFF3CT, so you will need to get a newer version of cmake.

    wget https://github.com/Kitware/CMake/releases/download/v3.18.2/cmake-3.18.2.tar.gz
    tar -xvzf cmake-3.18.2.tar.gz 
    pushd cmake-3.18.2/
    ./bootstrap
    make
    sudo make install
    popd 
    rm -rf cmake-3.18-2

## Debugging in VS Code

I like using VS Code for C/C++ development and debugging.  I use the following plugins to facilitate my development:

* [C/C++ Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
* [C++ Intellisense](https://marketplace.visualstudio.com/items?itemName=austin.code-gnu-global)
* [CMake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
* [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
* [CMake Tools Helper](https://marketplace.visualstudio.com/items?itemName=maddouri.cmake-tools-helper)
* [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)

To debug in VS Code
1) Checkout and build the project as described above.
2) Open the top-level dvbs2_fec_processor folder in VS Code.
3) When prompted by VS Code, select "GCC 9.2.1 Using compilers: C = /bin/gcc-9, CXX = /bin/g++-9" (I use Control-Shift-P to run commands).  VS Code may run this automatically when it sees a CMake project.
4) Run "CMake: Set Build Target" and select "dvbs2_fec_processor_example" (Sometimes I have to do "CMake: Configure" before this step to get the list of targets to populate)
5) Run "CMake: Set Debug Target" and select "dvbs2_fec_processor_example"
6) Debug -> Start Debugging, select C++ (GDB/LLDB)
7) The launch.json file should open.  Modify the contents to include:
    ```code
    "program": "${command:cmake.launchTargetPath}",
    "args": ["--zmq-endpoint", "tcp://*:9000", "--input-filename", "/home/ben/dev/gitlab/dvbs2_demod/llr.bin", "--output-filename", "/home/ben/dev/gitlab/dvbs2_demod/bits.bin", "-K", "14400", "--enc-cw-size", "16200", "--enc-type", "LDPC_DVBS2"],
   ```
8) Debug -> Start Debugging

Note that I sometimes have to make a small change to the dvbs2_fec_processor_example/src/main.cpp file to get VS Code to recompile and respect my breakpoints.  No idea why.


## Running the Project

The example application can be run with:

	./apps/example/bin/dvbs2_fec_processor_example --zmq-endpoint "tcp://*:9000" --input-filename "/home/ben/dev/gitlab/dvbs2_demod/llr.bin" --output-filename "/home/ben/dev/gitlab/dvbs2_demod/bits.bin" -K 14400 --enc-cw-size 16200 --enc-type LDPC_DVBS2
	
## Python Support and Integration

The package in `python/py_dvbs2_fec` wraps the compiled C/C++ library and provides a means of accessing the functionality from within a Python environment.  

## MATLAB Support and Integration
The `matlab/DVBS2FECProcessing.m` file contains a class that wraps the compiled C/C++ library and provides a means of accessing the functionality from within a MATLAB environment.  This is useful for verifying AFF3CT and DVB-S2 FEC Processing functionality against a MATLAB reference, and for comparing performance between the two implementations.  An example of using this wrapper class is in `matlab/DVBS2FECProcessingExample.m`.

Prior to launching MATLAB you may have to set the LD_PRELOAD environment variable to point to the version of libstdc++ that was used to compile the DVB-S2 FEC Processor code.  On Ubuntu 19.10, the following command accomplishes this:
```code
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.28
```

A MATLAB Unit Test class is also provided in `matlab/DVBS2FECProcessingTest.m` that will check the two implementations for each DVB-S2 ModCod.  This test class sets the Es/N0 to 3 dB above the threshold specified in Table 13 of ETSI EN 302 307 V1.2.1 and effectively checks the core functionality of the BCH and LDPC encoders and decoders at an Es/N0 that is sufficiently high enough to avoid any small performance differences near the threshold Es/N0.

```matlab
suite = matlab.unittest.TestSuite.fromFile(...
'./dvbs2_fec_processor/matlab/DVBS2FECProcessingTest.m');
suite.run
```


## Other Stuff - Unsorted and Possibly Old/Obsolete

### Compiling on Windows (Used to Work)
To compile the library on Windows (Visual Studio project):

	pushd lib/aff3ct
	mkdir build
	cd build
	cmake .. -G"Visual Studio 16 2019" -DCMAKE_CXX_FLAGS="-D_CRT_SECURE_NO_DEPRECATE /EHsc /MP4 /bigobj" -DAFF3CT_COMPILE_EXE="OFF" -DAFF3CT_COMPILE_STATIC_LIB="ON" -DAFF3CT_COMPILE_SHARED_LIB="ON"
	devenv /build Debug aff3ct.sln
	popd

### Other Assorted Old/Obsolete Notes:
Update CMakeLists.txt to point to the installation:

	set (ZeroMQ_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../thirdparty/vcpkg/packages/zeromq_x64-windows-static/share/zeromq/")
	find_package(ZeroMQ REQUIRED)
	target_link_libraries(dvbs2_fec_processor PRIVATE ${ZeroMQ_LIBRARY})

To debug using Visual Studio, in the Solution Explorer you will have to right click on dvbs2_fec_processor and select "Set as StartUp Project" before running. 

Command line arguments:

	-K 48600 --enc-cw-size 64800 --chn-fra-size 43200 --mdm-fra-size 64800 -m 0.1 -M 10.0 --enc-type LDPC_DVBS2 --mdm-type PSK --mdm-bps 3
	
VS Code launch.json notes:

    "program": "${command:cmake.launchTargetPath}",
    "args": ["--zmq-endpoint", "tcp://*:9000", "--input-filename", "/home/ben/dev/gitlab/dvbs2_demod/llr.bin", "--output-filename", "/home/ben/dev/gitlab/dvbs2_demod/bits.bin", "-K", "14400", "--enc-cw-size", "16200", "--enc-type", "LDPC_DVBS2"],

