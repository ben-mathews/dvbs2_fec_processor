#!/bin/bash
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/4,QPSK_3/5,QPSK_5/6   --frame_length normal  --ldpc-dec-ite 50
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/3,QPSK_2/3,QPSK_8/9   --frame_length normal  --ldpc-dec-ite 50
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_2/5,QPSK_3/4,QPSK_9/10  --frame_length normal  --ldpc-dec-ite 50
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/2,QPSK_4/5            --frame_length normal  --ldpc-dec-ite 50

/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod 8PSK_3/5,8PSK_5/6   --frame_length normal  --ldpc-dec-ite 50
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod 8PSK_2/3,8PSK_8/9   --frame_length normal  --ldpc-dec-ite 50
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod 8PSK_3/4,8PSK_9/10  --frame_length normal  --ldpc-dec-ite 50

#!/bin/bash
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/4,QPSK_3/5,QPSK_5/6,8PSK_3/5           --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/3,QPSK_2/3,QPSK_8/9,8PSK_2/3           --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_2/5,QPSK_3/4,QPSK_9/10,8PSK_3/4          --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_1/2,QPSK_4/5,8PSK_5/6,8PSK_8/9,8PSK_9/10 --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100


/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_3/5,QPSK_4/5,QPSK_5/6,8PSK_3/5             --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_2/3,8PSK_5/6,QPSK_8/9,8PSK_2/3             --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100
/home/ben/dev/gitlab/dvbs2_fec_processor/build/apps/example_simulation/bin/dvbs2_fec_processor_example_simulation --modcod QPSK_3/4,QPSK_9/10,8PSK_8/9,8PSK_3/4,8PSK_9/10  --frame_length normal  --ldpc-dec-ite 50 --num_sim_iterations 100



