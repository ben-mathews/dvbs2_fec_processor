import os
import matplotlib.pyplot as plt
import numpy as np
from aff3ct_trace_reader import aff3ctTraceReader

plt.style.use('dark_background')

folder = '/home/ben/dev/gitlab/dvbs2_fec_processor'

modcods = ['QPSK_1_4', 'QPSK_1_3', 'QPSK_2_5', 'QPSK_1_2', 'QPSK_3_5', 'QPSK_2_3', 'QPSK_3_4', 'QPSK_4_5', 'QPSK_5_6', 'QPSK_8_9', 'QPSK_9_10', '8PSK_3_5', '8PSK_2_3', '8PSK_3_4', '8PSK_5_6', '8PSK_8_9', '8PSK_9_10']

filenames = os.listdir(folder)

plt.figure()
for modcod in modcods:
    modcod_indicies = np.where(np.asarray([filename.find(modcod) for filename in filenames]) > -1)
    if len(modcod_indicies[0]) != 1:
        continue

    filename = os.path.join(folder, filenames[modcod_indicies[0][0]])

    affect_file = aff3ctTraceReader(aff3ctOutput=filename)
    if len(affect_file.getTrace("n_fra")) == 0:
        continue
    num_frames = affect_file.getTrace("n_fra")[0]

    mod_type = None
    if affect_file.getSimuHeader('Bits per symbol') == '2':
        mod_type = 'QPSK'
    elif affect_file.getSimuHeader('Bits per symbol') == '3':
        mod_type = '8PSK'
    else:
        raise Exception('Unhandled Bits per symbol')

    code_rate = affect_file.getSimuHeader('Code rate')
    code_rate = code_rate.split(' ')[1].replace('(', '').replace(')', '')

    qef_esn0 = np.nan
    if np.sum(affect_file.getTrace("fe_rate") == 1/num_frames) > 0:
        qef_esn0 = affect_file.getTrace("esn0")[np.min(np.where(affect_file.getTrace("fe_rate") == 1/num_frames))]

    label = mod_type + ' ' + code_rate + ' ({:2.2f} dB)'.format(qef_esn0)

    plt.semilogy(affect_file.getTrace("esn0"), affect_file.getTrace("fe_rate"), label=label)

plt.legend()
plt.show(block=False)



ttt = 1