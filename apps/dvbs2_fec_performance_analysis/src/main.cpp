#include <functional>
#include <exception>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <memory>
#include <vector>
#include <string>
#include<sys/types.h>
#include <map>

using namespace std;

#include <aff3ct.hpp>
using namespace aff3ct;
using namespace tools;

#include "DVBS2_FEC_Processor.h"

struct params
{
	int   K = -1;     // number of information bits
	int   N = -1;     // codeword size
	int   K_BCH = -1;     // codeword size
	int   N_BCH_K_LDPC = -1;     // codeword size
	int   N_LDPC = -1;
	int   BitsPerSymbol = -1;
	float R;                  // code rate (R=K/N)

	std::unique_ptr<factory::Codec_LDPC> codec_params_ldpc;
    std::unique_ptr<factory::Codec_BCH>  codec_params_bch;

	std::unique_ptr<factory::Source          > source;
	std::unique_ptr<factory::Modem           > modem;
	std::unique_ptr<factory::Channel         > channel;
	std::unique_ptr<factory::Monitor_BFER    > monitor;
	std::unique_ptr<factory::Terminal        > terminal;
};
void init_params(int argc, char** argv, params& p, std::ostream &stream = std::cout);

struct modules
{
	std::unique_ptr<module::Source<>>       source;
	std::unique_ptr<module::Modem<>>        modem;
	std::unique_ptr<module::Channel<>>      channel;
	std::unique_ptr<module::Monitor_BFER<>> monitor;
	                module::Encoder<>*      encoder;
	std::vector<const module::Module*>      list; // list of module pointers declared in this structure
};
void init_modules(const params &p, modules &m);

struct buffers
{
	std::vector<int  > ref_bits;
	std::vector<int  > enc_bits_bch;
	std::vector<int  > enc_bits_ldpc;
	std::vector<float> symbols;
	std::vector<float> noisy_symbols;
	std::vector<float> LLRs;
	std::vector<int  > dec_bits_ldpc_cw;
	std::vector<int  > dec_bits_ldpc;
	std::vector<int  > dec_bits_bch_cw;
	std::vector<int  > dec_bits_bch;
};
void init_buffers(const params &p, buffers &b);

struct utils
{
	std::unique_ptr<tools::Sigma<>>               noise;     // a sigma noise type
	std::vector<std::unique_ptr<tools::Reporter>> reporters; // list of reporters dispayed in the terminal
	std::unique_ptr<tools::Terminal>              terminal;  // manage the output text in the terminal
};
void init_utils(const params &p, const modules &m, utils &u);


int process_command_line_arguments(int& argc, char*** argv, std::string& input_filename, std::string& output_filename, std::string& zmq_endpoint, int& udp_server_port, bool& retflag);

map<string, vector<string>> dvbs2_params_set_normal = {
    {"QPSK_1/4",  {"", "-N", "64800", "-K", "16008", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "16200", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "16008", "--bch-enc-cw-size", "16200", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_1/3",  {"", "-N", "64800", "-K", "21408", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "21600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "21408", "--bch-enc-cw-size", "21600", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_2/5",  {"", "-N", "64800", "-K", "25728", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "25920", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "25728", "--bch-enc-cw-size", "25920", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_1/2",  {"", "-N", "64800", "-K", "32208", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "32400", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "32208", "--bch-enc-cw-size", "32400", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_3/5",  {"", "-N", "64800", "-K", "38688", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "38880", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "38688", "--bch-enc-cw-size", "38880", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_2/3",  {"", "-N", "64800", "-K", "43040", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "43200", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "43040", "--bch-enc-cw-size", "43200", "--bch-dec-corr-pow", "10"}}, 
	{"QPSK_3/4",  {"", "-N", "64800", "-K", "48408", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "48600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "48408", "--bch-enc-cw-size", "48600", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_4/5",  {"", "-N", "64800", "-K", "51648", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "51840", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "51648", "--bch-enc-cw-size", "51840", "--bch-dec-corr-pow", "12"}}, 
	{"QPSK_5/6",  {"", "-N", "64800", "-K", "53840", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "54000", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "53840", "--bch-enc-cw-size", "54000", "--bch-dec-corr-pow", "10"}}, 
	{"QPSK_8/9",  {"", "-N", "64800", "-K", "57472", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "57600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "57472", "--bch-enc-cw-size", "57600", "--bch-dec-corr-pow", "8"}}, 
	{"QPSK_9/10", {"", "-N", "64800", "-K", "58192", "--mdm-type", "PSK", "--mdm-bps", "2", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "64800", "--ldpc-enc-info-bits", "58320", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "58192", "--bch-enc-cw-size", "58320", "--bch-dec-corr-pow", "8"}}, 
    {"8PSK_1/4",  {"", "-N", "64800", "-K", "16008", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "16200", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "16008", "--bch-enc-cw-size", "16200", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_1/3",  {"", "-N", "64800", "-K", "21408", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "21600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "21408", "--bch-enc-cw-size", "21600", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_2/5",  {"", "-N", "64800", "-K", "25728", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "25920", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "25728", "--bch-enc-cw-size", "25920", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_1/2",  {"", "-N", "64800", "-K", "32208", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "32400", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "32208", "--bch-enc-cw-size", "32400", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_3/5",  {"", "-N", "64800", "-K", "38688", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "38880", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "38688", "--bch-enc-cw-size", "38880", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_2/3",  {"", "-N", "64800", "-K", "43040", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "43200", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "43040", "--bch-enc-cw-size", "43200", "--bch-dec-corr-pow", "10"}}, 
	{"8PSK_3/4",  {"", "-N", "64800", "-K", "48408", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "48600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "48408", "--bch-enc-cw-size", "48600", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_4/5",  {"", "-N", "64800", "-K", "51648", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "51840", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "51648", "--bch-enc-cw-size", "51840", "--bch-dec-corr-pow", "12"}}, 
	{"8PSK_5/6",  {"", "-N", "64800", "-K", "53840", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "54000", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "53840", "--bch-enc-cw-size", "54000", "--bch-dec-corr-pow", "10"}}, 
	{"8PSK_8/9",  {"", "-N", "64800", "-K", "57472", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "57600", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "57472", "--bch-enc-cw-size", "57600", "--bch-dec-corr-pow", "8"}}, 
	{"8PSK_9/10", {"", "-N", "64800", "-K", "58192", "--mdm-type", "PSK", "--mdm-bps", "3", "--mdm-cpm-ws", "RCOS", "--chn-fra-size", "43200", "--ldpc-enc-info-bits", "58320", "--ldpc-enc-cw-size", "64800", "--ldpc-enc-type", "LDPC_DVBS2", "--ldpc-dec-ite", "50", "--bch-enc-info-bits", "58192", "--bch-enc-cw-size", "58320", "--bch-dec-corr-pow", "8"}}, 
    };
map<string, float> dvbs2_qef_esn0_normal= {
    {"QPSK_1/4",  -2.35},
	{"QPSK_1/3",  -1.24},
	{"QPSK_2/5",  -0.30}, 
	{"QPSK_1/2",  1.00}, 
	{"QPSK_3/5",  2.23}, 
	{"QPSK_2/3",  3.10}, 
	{"QPSK_3/4",  4.03}, 
	{"QPSK_4/5",  4.68}, 
	{"QPSK_5/6",  5.18}, 
	{"QPSK_8/9",  6.20}, 
	{"QPSK_9/10", 6.42}, 
	{"8PSK_3/5",  5.50}, 
	{"8PSK_2/3",  6.62}, 
	{"8PSK_3/4",  7.91}, 
	{"8PSK_5/6",  9.35}, 
	{"8PSK_8/9",  10.69}, 
	{"8PSK_9/10", 10.98}, 
    };

int main(int argc, char** argv)
{
	// get the AFF3CT version
	const std::string v = "v" + std::to_string(tools::version_major()) + "." +
		std::to_string(tools::version_minor()) + "." +
		std::to_string(tools::version_release());

	cout << "Called: ";
	for (int i = 0; i < argc; ++i)
		cout << argv[i] << " ";
	cout << "\n";

	std::vector<std::string> argList(argv, argv + argc);
	auto modcod = std::find(argList.begin(), argList.end(), "--modcod");
	auto frame_length = std::find(argList.begin(), argList.end(), "--frame_length");
	auto ldpc_dec_ite = std::find(argList.begin(), argList.end(), "--ldpc-dec-ite");
	auto num_sim_iterations_itr = std::find(argList.begin(), argList.end(), "--num_sim_iterations");
	if (modcod != argList.end())
		modcod++;
	else
		throw std::invalid_argument( "Missing --modcod option" );
	if (frame_length != argList.end())
		frame_length++;
	else
		throw std::invalid_argument( "Missing --frame_length option" );
	string ldpc_dec_ite_string = "";
	if (ldpc_dec_ite != argList.end())
	{
		ldpc_dec_ite++;
		ldpc_dec_ite_string = (string)(*ldpc_dec_ite);
	}
	int num_sim_iterations = 100;
	if (num_sim_iterations_itr != argList.end())
	{
		num_sim_iterations_itr++;
		num_sim_iterations = stoi((string)(*num_sim_iterations_itr));
	}

	
	vector<string> modcods_str;
	std::stringstream ss((string)(*modcod));

	while(ss.good())
	{
		string substr;
		getline( ss, substr, ',' );
		std::string::iterator end_pos = std::remove(substr.begin(), substr.end(), ' ');
		substr.erase(end_pos, substr.end());
		modcods_str.push_back( substr );		
	}

	for (vector<string>::iterator modcod_itr=modcods_str.begin(); modcod_itr!=modcods_str.end(); ++modcod_itr) 
    {
		if (dvbs2_params_set_normal.find((string)(*modcod_itr)) == dvbs2_params_set_normal.end())
			throw std::invalid_argument( "Specified modcod can't be found" );

		string modcod_str = (string)(*modcod_itr);
		auto dvbs2_params = dvbs2_params_set_normal[modcod_str];
		float qef_esn0_db = dvbs2_qef_esn0_normal[modcod_str];

		ofstream logfile;
		string logfilename = modcod_str + "_log.txt";
		std::replace(logfilename.begin(), logfilename.end(), '/', '_');
		logfile.open(logfilename);
		
		dvbs2_params[0] = argv[0];

		if (ldpc_dec_ite_string != "")
		{
			int str_index = 0;
			for (; str_index < dvbs2_params.size(); str_index++)
				if (dvbs2_params[str_index] == "--ldpc-dec-ite")
					break;
			dvbs2_params[++str_index] = ldpc_dec_ite_string;
		}

		argc = dvbs2_params.size();
		vector<char*> argv_custom;
		argv_custom.reserve(dvbs2_params.size());
		for(size_t i = 0; i < dvbs2_params.size(); ++i)
			argv_custom.push_back(const_cast<char*>(dvbs2_params[i].c_str()));

		std::vector<float> llr_symbols(64800);

		DVBS2_FEC_Processor* dvb_proc = new DVBS2_FEC_Processor(argc, argv_custom.data());

		params  p; init_params(argc, argv_custom.data(), p, logfile);  // create and initialize the parameters from the command line with factories
		p.K_BCH = dvb_proc->K_BCH;
		p.N_BCH_K_LDPC = dvb_proc->N_BCH_K_LDPC;
		p.N_LDPC = dvb_proc->N_LDPC;
		p.BitsPerSymbol = p.modem.get()->bps;
		modules m; init_modules(p, m         ); // create and initialize the modules
		buffers b; init_buffers(p, b         ); // create and initialize the buffers required by the modules
		utils   u; init_utils  (p, m, u      ); // create and initialize the utils

		u.terminal->legend();
		u.terminal->legend(logfile);

		float esn0_start = qef_esn0_db -3.0;
		float esn0_end = qef_esn0_db + 3.0;
		//esn0_start = 6;
		//esn0_end = 30;
		for (float esn0 = esn0_start; esn0 < esn0_end; esn0 += 0.1)
		{
			const auto ebn0  = tools::esn0_to_ebn0(esn0, p.R);
			const auto sigma = tools::esn0_to_sigma(esn0    );

			u.noise->set_noise(sigma, ebn0, esn0);

			// update the sigma of the modem and the channel
			m.modem  ->set_noise(*u.noise);
			m.channel->set_noise(*u.noise);

			dvb_proc->set_esn0(esn0);

			u.terminal->start_temp_report();

			for (int n_iter = 0; n_iter < num_sim_iterations; n_iter++)
			{
				m.source ->generate    (                 b.ref_bits     );
				dvb_proc->encode_bch   (b.ref_bits,      b.enc_bits_bch);
				dvb_proc->encode_ldpc  (b.enc_bits_bch,  b.enc_bits_ldpc);
				m.modem->modulate      (b.enc_bits_ldpc, b.symbols);
				m.channel->add_noise   (b.symbols,       b.noisy_symbols);
				m.modem->demodulate    (b.noisy_symbols, b.LLRs);
				bool ldpc_pass = dvb_proc->decode_ldpc  (b.LLRs, b.dec_bits_ldpc_cw);
				b.dec_bits_ldpc = std::vector<int>(b.dec_bits_ldpc_cw.begin(), b.dec_bits_ldpc_cw.begin() + p.N_BCH_K_LDPC);
				dvb_proc->decode_bch   (b.dec_bits_ldpc, b.dec_bits_bch_cw);
				b.dec_bits_bch = std::vector<int>(b.dec_bits_bch_cw.begin(), b.dec_bits_bch_cw.begin() + p.K_BCH);
				m.monitor->check_errors(b.ref_bits,      b.dec_bits_bch);

				bool log_raw_data = true;
				if (log_raw_data)
				{
					std::ofstream LLRs_log("LLRs.bin",std::ios_base::binary);
					if(LLRs_log.good())
					{
						LLRs_log.write((char *)&b.LLRs[0],b.LLRs.size()*sizeof(float));
						LLRs_log.close();
					}
					std::ofstream symbols_log("symbols.bin",std::ios_base::binary);
					if(symbols_log.good())
					{
						symbols_log.write((char *)&b.symbols[0],b.symbols.size()*sizeof(float));
						symbols_log.close();
					}
					std::ofstream noisy_symbols_log("noisy_symbols.bin",std::ios_base::binary);
					if(symbols_log.good())
					{
						noisy_symbols_log.write((char *)&b.noisy_symbols[0],b.noisy_symbols.size()*sizeof(float));
						noisy_symbols_log.close();
					}
				}


				dvb_proc->Reset();
			}

			u.terminal->final_report();
			u.terminal->final_report(logfile);

			// reset the monitor for the next SNR
			m.monitor->reset();
			u.terminal->reset();

			// if user pressed Ctrl+c twice, exit the SNRs loop
			if (u.terminal->is_over()) break;
		}
	}

	return true;
}


void init_params(int argc, char** argv, params& p, std::ostream &stream)
{
	p.codec_params_ldpc = std::unique_ptr<factory::Codec_LDPC>(new factory::Codec_LDPC());
    p.codec_params_bch =  std::unique_ptr<factory::Codec_BCH>(new factory::Codec_BCH());

	p.source   = std::unique_ptr<factory::Source          >(new factory::Source          ());
	p.modem    = std::unique_ptr<factory::Modem           >(new factory::Modem           ());
	p.channel  = std::unique_ptr<factory::Channel         >(new factory::Channel         ());
	p.monitor  = std::unique_ptr<factory::Monitor_BFER    >(new factory::Monitor_BFER    ());
	p.terminal = std::unique_ptr<factory::Terminal        >(new factory::Terminal        ());

	std::vector<factory::Factory*> params_list_ldpc = { p.codec_params_ldpc.get() };
	std::vector<factory::Factory*> params_list_bch = { p.codec_params_bch.get() };

	std::vector<factory::Factory*> params_list = { p.source .get(), p.modem   .get(),
	                                               p.channel.get(), p.monitor.get(), p.terminal.get() };
	tools::Command_parser cp(argc, argv, params_list, true);

	char** argv_ldpc;
	char** argv_bch;
	std::vector<std::string> argList(argv, argv + argc);

	vector<char*> pointer_vec_ldpc(1);
	vector<char*> pointer_vec_bch(1);
	pointer_vec_ldpc[0] = (char*) argList[0].data();
	pointer_vec_bch[0] = (char*) argList[0].data();
	for(unsigned i = 1; i < argList.size(); i+=2)
	{
		int ldpc_index = argList[i].find("ldpc-");
		int bch_index = argList[i].find("bch-");
		if (ldpc_index >= 0)
		{
			argList[i].replace(ldpc_index, 5, "");
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i].data());
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i+1].data());
		}
		else if (bch_index >= 0)
		{
			argList[i].replace(bch_index, 4, "");
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i].data());
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i+1].data());
		}
		else
		{
			/* code */
		}
	} //you can use transform instead of this loop
	argv_ldpc = pointer_vec_ldpc.data();
	argv_bch = pointer_vec_bch.data();
	int argc_ldpc = pointer_vec_ldpc.size();
	int argc_bch = pointer_vec_bch.size();
    
	tools::Command_parser cp_ldpc(argc_ldpc, argv_ldpc, params_list_ldpc, true);
	if (argc_ldpc > 1 & cp_ldpc.parsing_failed())
	{
		cp_ldpc.print_help();
		cp_ldpc.print_warnings();
		cp_ldpc.print_errors();
		std::exit(1);
	}
	cp_ldpc.print_warnings();
	
	tools::Command_parser cp_bch(argc_bch, argv_bch, params_list_bch, true);
	if (argc_bch > 1 & cp_bch.parsing_failed())
	{
		cp_bch.print_help();
		cp_bch.print_warnings();
		cp_bch.print_errors();
		std::exit(1);
	}
	cp_bch.print_warnings();

	std::cout << "# Simulation parameters: " << std::endl;
	tools::Header::print_parameters(params_list, true, stream); // display the headers (= print the AFF3CT parameters on the screen)
	std::cout << "#" << std::endl << std::endl;

	std::cout << "# LDPC parameters: " << std::endl;
	tools::Header::print_parameters(params_list_ldpc, true, stream); // display the headers (= print the AFF3CT parameters on the screen)
	std::cout << "#" << std::endl << std::endl;

	std::cout << "# BCH parameters: " << std::endl;
	tools::Header::print_parameters(params_list_bch, true, stream); // display the headers (= print the AFF3CT parameters on the screen)
	std::cout << "#" << std::endl << std::endl;

	p.K = (float)p.codec_params_ldpc->enc->K;
	p.N = (float)p.codec_params_ldpc->enc->N_cw;
	p.R = (float)p.codec_params_ldpc->enc->K / (float)p.codec_params_ldpc->enc->N_cw; // compute the code rate
}


void init_modules(const params &p, modules &m)
{
	m.source  = std::unique_ptr<module::Source      <>>(p.source ->build());
	m.modem   = std::unique_ptr<module::Modem       <>>(p.modem  ->build());
	m.channel = std::unique_ptr<module::Channel     <>>(p.channel->build());
	m.monitor = std::unique_ptr<module::Monitor_BFER<>>(p.monitor->build());

	m.list = { m.source.get(), m.modem.get(), m.channel.get(), m.monitor.get() };

	// configuration of the module tasks
	for (auto& mod : m.list)
		for (auto& tsk : mod->tasks)
		{
			tsk->set_autoalloc  (true ); // enable the automatic allocation of the data in the tasks
			tsk->set_autoexec   (false); // disable the auto execution mode of the tasks
			tsk->set_debug      (false); // disable the debug mode
			tsk->set_debug_limit(16   ); // display only the 16 first bits if the debug mode is enabled
			tsk->set_stats      (true ); // enable the statistics

			// enable the fast mode (= disable the useless verifs in the tasks) if there is no debug and stats modes
			if (!tsk->is_debug() && !tsk->is_stats())
				tsk->set_fast(true);
		}

	// reset the memory of the decoder after the end of each communication
	// m.monitor->add_handler_check(std::bind(&DVBS2_FEC_Processor::Reset, Processor));
}


void init_buffers(const params &p, buffers &b)
{
	b.ref_bits         = std::vector<int  >(p.K_BCH);
	b.enc_bits_bch     = std::vector<int  >(p.N_BCH_K_LDPC);
	b.enc_bits_ldpc    = std::vector<int  >(p.N_LDPC);
	b.symbols          = std::vector<float>(2 * p.N / p.BitsPerSymbol);
	b.noisy_symbols    = std::vector<float>(2 * p.N / p.BitsPerSymbol);
	b.LLRs             = std::vector<float>(p.N);
	b.dec_bits_ldpc_cw = std::vector<int  >(p.N_LDPC);
	b.dec_bits_ldpc    = std::vector<int>(p.N_BCH_K_LDPC);
	b.dec_bits_bch_cw  = std::vector<int>(p.N_BCH_K_LDPC);
	b.dec_bits_bch     = std::vector<int>(p.K_BCH);
}


void init_utils(const params &p, const modules &m, utils &u)
{
	// create a sigma noise type
	u.noise = std::unique_ptr<tools::Sigma<>>(new tools::Sigma<>());
	// report the noise values (Es/N0 and Eb/N0)
	u.reporters.push_back(std::unique_ptr<tools::Reporter>(new tools::Reporter_noise<>(*u.noise)));
	// report the bit/frame error rates
	u.reporters.push_back(std::unique_ptr<tools::Reporter>(new tools::Reporter_BFER<>(*m.monitor)));
	// report the simulation throughputs
	u.reporters.push_back(std::unique_ptr<tools::Reporter>(new tools::Reporter_throughput<>(*m.monitor)));
	// create a terminal that will display the collected data from the reporters
	u.terminal = std::unique_ptr<tools::Terminal>(p.terminal->build(u.reporters));
}