#include <functional>
#include <exception>
#include <iostream>
#include <cstdlib>
#include <memory>
#include <vector>
#include <string>
#include<sys/types.h>

using namespace std;

#include <aff3ct.hpp>
using namespace aff3ct;
using namespace tools;

#include "DVBS2_FEC_Processor.h"

struct params
{
	int   K = -1;     // number of information bits
	int   N = -1;     // codeword size
	float R;                  // code rate (R=K/N)

	std::unique_ptr<factory::Codec_LDPC> codec_params_ldpc;
    std::unique_ptr<factory::Codec_BCH>  codec_params_bch;
};
void init_params(int argc, char** argv, params& p);

int process_command_line_arguments(int& argc, char*** argv, std::string& input_filename, std::string& output_filename, bool& retflag);

int main(int argc, char** argv)
{
	// get the AFF3CT version
	const std::string v = "v" + std::to_string(tools::version_major()) + "." +
		std::to_string(tools::version_minor()) + "." +
		std::to_string(tools::version_release());

	cout << "Called: ";
	for (int i = 0; i < argc; ++i)
		cout << argv[i] << " ";
	cout << "\n";

	bool retflag;
	string input_filename;
	string output_filename;
	int retval = process_command_line_arguments(argc, &argv, input_filename, output_filename, retflag);
	if (retflag) return retval;

	std::ifstream input_file(input_filename.c_str(), std::ios::in);
	std::vector<float> llr_symbols(16200);
	if (input_file.is_open())
	{
		input_file.read(reinterpret_cast<char*>(llr_symbols.data()), llr_symbols.size()*sizeof(float)); // 
		input_file.close();
	}
	else
	{
		std::cout << "File not opened" << std::endl;
		return 0;
	}

	DVBS2_FEC_Processor* dvb_proc = new DVBS2_FEC_Processor(argc, argv);

	params  p; 
	init_params(argc, argv, p); // create and initialize the parameters from the command line with factories

	bool Test_BCH_Encoder = true;
	if (Test_BCH_Encoder)
	{
		std::vector<int> bch_cw       = std::vector<int>(p.codec_params_bch->enc->N_cw);
		std::vector<int> bch_user_bits = std::vector<int>(p.codec_params_bch->enc->K);
		bool passed = dvb_proc->encode_bch(bch_user_bits, bch_cw);
	}

	std::vector<int> cw       = std::vector<int  >(p.N);
	std::vector<int> dec_bits = std::vector<int  >(p.K);
	bool passed = dvb_proc->decode_ldpc(llr_symbols, cw);
	
	std::ofstream output_file(output_filename.c_str(), std::ios::out);
	if (output_file.is_open())
	{
		output_file.write(reinterpret_cast<char*>(dec_bits.data()), dec_bits.size()*sizeof(int)); // 
		output_file.close();
	}
	else
	{
		std::cout << "File not opened" << std::endl;
		return 0;
	}

	return passed;
}

int process_command_line_arguments(int& argc, char*** argv, std::string& input_filename, std::string& output_filename, bool& retflag)
{
	char** new_argv = new char*[argc];
	int new_argc = 0;
	retflag = true;
	// Process and strip off non-AA3CT command line arguments
	new_argv[0] = (*argv)[new_argc++];
	for (int i = 1; i < argc; i+=2)
		if (std::string((*argv)[i]) == "--input-filename") {
			if (i + 1 < argc) {
				input_filename = (*argv)[i+1];
			}
			else {
				std::cerr << "--destination option requires one argument." << std::endl;
				return 1;
			}
		}
		else if (std::string((*argv)[i]) == "--output-filename") {
			if (i + 1 < argc) {
				output_filename = (*argv)[i+1];
			}
			else {
				std::cerr << "--destination option requires one argument." << std::endl;
				return 1;
			}
		}
		else
		{
			new_argv[new_argc++] = (*argv)[i];
			new_argv[new_argc++] = (*argv)[i+1];
		}
		
	retflag = false;
	*argv = new_argv;
	// *argv = NULL;
	argc = new_argc;
	return {};
}

void init_params(int argc, char** argv, params& p)
{
	p.codec_params_ldpc = std::unique_ptr<factory::Codec_LDPC>(new factory::Codec_LDPC());
    p.codec_params_bch =  std::unique_ptr<factory::Codec_BCH>(new factory::Codec_BCH());
	std::vector<factory::Factory*> params_list_ldpc = { p.codec_params_ldpc.get() };
	std::vector<factory::Factory*> params_list_bch = { p.codec_params_bch.get() };

	char** argv_ldpc;
	char** argv_bch;
	std::vector<std::string> argList(argv, argv + argc);

	vector<char*> pointer_vec_ldpc(1);
	vector<char*> pointer_vec_bch(1);
	pointer_vec_ldpc[0] = (char*) argList[0].data();
	pointer_vec_bch[0] = (char*) argList[0].data();
	for(unsigned i = 1; i < argList.size(); i+=2)
	{
		int ldpc_index = argList[i].find("ldpc-");
		int bch_index = argList[i].find("bch-");
		if (ldpc_index >= 0)
		{
			argList[i].replace(ldpc_index, 5, "");
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i].data());
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i+1].data());
		}
		else if (bch_index >= 0)
		{
			argList[i].replace(bch_index, 4, "");
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i].data());
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i+1].data());
		}
		else
		{
			/* code */
		}
	} //you can use transform instead of this loop
	argv_ldpc = pointer_vec_ldpc.data();
	argv_bch = pointer_vec_bch.data();
	int argc_ldpc = pointer_vec_ldpc.size();
	int argc_bch = pointer_vec_bch.size();
    
	tools::Command_parser cp_ldpc(argc_ldpc, argv_ldpc, params_list_ldpc, true);
	if (argc_ldpc > 1 & cp_ldpc.parsing_failed())
	{
		cp_ldpc.print_help();
		cp_ldpc.print_warnings();
		cp_ldpc.print_errors();
		std::exit(1);
	}
	cp_ldpc.print_warnings();
	
	tools::Command_parser cp_bch(argc_bch, argv_bch, params_list_bch, true);
	if (argc_bch > 1 & cp_bch.parsing_failed())
	{
		cp_bch.print_help();
		cp_bch.print_warnings();
		cp_bch.print_errors();
		std::exit(1);
	}
	cp_bch.print_warnings();

	std::cout << "# LDPC parameters: " << std::endl;
	tools::Header::print_parameters(params_list_ldpc, true); // display the headers (= print the AFF3CT parameters on the screen)
	std::cout << "#" << std::endl;

	p.K = (float)p.codec_params_ldpc->enc->K;
	p.N = (float)p.codec_params_ldpc->enc->N_cw;
	p.R = (float)p.codec_params_ldpc->enc->K / (float)p.codec_params_ldpc->enc->N_cw; // compute the code rate
}
