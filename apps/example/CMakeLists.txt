cmake_minimum_required(VERSION 3.2)
cmake_policy(SET CMP0054 NEW)

project (dvbs2_fec_processor_example)

# ---------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------- CMAKE OPTIONS
# ---------------------------------------------------------------------------------------------------------------------

# Enable C++11
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Specify bin path
set (EXECUTABLE_OUTPUT_PATH bin/)

# Link with the "Threads library (required to link with AFF3CT after)
set(CMAKE_THREAD_PREFER_PTHREAD ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

# Link with ZMQ
#set (ZeroMQ_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../thirdparty/vcpkg/packages/zeromq_x64-windows-static/share/zeromq/")
#find_package(ZeroMQ REQUIRED)
#target_link_libraries(dvbs2_fec_processor_example PRIVATE ${ZeroMQ_LIBRARY})

## load in pkg-config support
find_package(PkgConfig)

#set (AFF3CT_DIR "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules/")
#find_package(AFF3CT CONFIG 2.3.2 REQUIRED)

# Create the executable from sources
add_executable(dvbs2_fec_processor_example ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp)
target_link_libraries(dvbs2_fec_processor_example PRIVATE aff3ct-shared-lib)
target_link_libraries(dvbs2_fec_processor_example PRIVATE dvbs2-fec-processor-shared-lib)
message(STATUS "dvbs2_fec_processor_example - Compile")

