#include <aff3ct.hpp>
using namespace aff3ct;

enum Frame_Length_Enum { Normal, Short };

enum FEC_Mode_Enum { LDPC, BCH, Both };



class DVBS2_FEC_Processor {
    public:
        DVBS2_FEC_Processor(int argc, char** argv);

        /*
        template <class AR = std::allocator<float>, class AB = std::allocator<int>>
	    bool process(const std::vector<float,AR>& Y_N, std::vector<int,AB>& V_N, const int frame_id = -1);
        */

        bool encode_ldpc(std::vector<int, std::allocator<int> > &U_K, std::vector<int, std::allocator<int> > &X_N, const int frame_id = -1);
        bool decode_ldpc(std::vector<float, std::allocator<float> > &Y_N, std::vector<int, std::allocator<int> >& V_N, const int frame_id = -1, const bool include_cw = false);
        bool encode_bch(std::vector<int, std::allocator<int> > &U_K, std::vector<int, std::allocator<int> >&X_N, const int frame_id = -1);
        bool decode_bch(std::vector<int, std::allocator<int> > &Y_N, std::vector<int, std::allocator<int> >& V_N, const int frame_id = -1, const bool include_cw = false);

        bool decode_ldpc_bch(std::vector<float, std::allocator<float> > &Y_N, std::vector<int, std::allocator<int> >& V_N, const int frame_id = -1);

        void set_esn0(const float esn0_db);

        void Reset();
        void DeleteObjects();

        int K_BCH;
        int N_BCH_K_LDPC;
        int N_BCH_Not_Shortened;
        int N_LDPC;
        int T_BCH;
        

    private:
    	int   K = -1;     // number of information bits
	    int   N = -1;     // codeword size

        Frame_Length_Enum Frame_Length;
        FEC_Mode_Enum FEC_Mode;

        std::unique_ptr<factory::Codec_LDPC>        codec_params_ldpc;
        std::unique_ptr<factory::Codec_BCH>         codec_params_bch;
       	std::unique_ptr<module::Codec_SIHO<>>                   codec_siho_ldpc;
        std::unique_ptr<module::Codec_SISO<>>                   codec_siso_ldpc;
        std::unique_ptr<module::Codec_HIHO<>>                   codec_hiho_bch;
        module::Encoder<>*                                      encoder_ldpc;
        module::Encoder<>*                                      encoder_bch;
        module::Decoder_SIHO<>*                                 decoder_siho_ldpc;
        module::Decoder_SISO<>*                                 decoder_siso_ldpc;
        module::Decoder_HIHO<>*                                 decoder_hiho_bch;
        std::unique_ptr<tools::Sigma<>>                         noise;     // a sigma noise type
        std::vector<const module::Module*>                      list; // list of module pointers declared in this structure

        int find_parameter(vector<char*> vec_str, const char* string_to_find);
};

extern "C" {
    DVBS2_FEC_Processor* DVBS2_FEC_Processor_Init(int argc, char** argv);
    void DVBS2_FEC_Processor_Reset(DVBS2_FEC_Processor* Processor);
    void DVBS2_FEC_Processor_DeleteObjects(DVBS2_FEC_Processor* Processor);
    void DVBS2_FEC_Processor_Set_EsN0(DVBS2_FEC_Processor* Processor, float esn0_db);
    bool DVBS2_FEC_Processor_Encode_LDPC(DVBS2_FEC_Processor* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id);
	bool DVBS2_FEC_Processor_Decode_LDPC(DVBS2_FEC_Processor* Processor, float Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw);
    bool DVBS2_FEC_Processor_Encode_BCH(DVBS2_FEC_Processor* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id);
    bool DVBS2_FEC_Processor_Decode_BCH(DVBS2_FEC_Processor* Processor, int Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw);
    bool DVBS2_FEC_Processor_Decode_LDPC_BCH(DVBS2_FEC_Processor* Processor, float Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id);
}
