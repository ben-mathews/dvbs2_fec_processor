//#include <aff3ct.hpp>
//using namespace aff3ct;


void* DVBS2_FEC_Processor_Init(int argc, char** argv);
void DVBS2_FEC_Processor_Reset(void* Processor);
void DVBS2_FEC_Processor_DeleteObjects(void* Processor);
bool DVBS2_FEC_Processor_Encode_LDPC(void* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id);
bool DVBS2_FEC_Processor_Decode_LDPC(void* Processor, float Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw);
bool DVBS2_FEC_Processor_Encode_BCH(void* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id);
bool DVBS2_FEC_Processor_Decode_BCH(void* Processor, int Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw);
bool DVBS2_FEC_Processor_Decode_LDPC_BCH(void* Processor, float Y_N[], int Num_Y_N, int X_N[], int Num_X_N, const int frame_id);
