using namespace std;
#include <stdio.h>
#include <stdlib.h>

#include "DVBS2_FEC_Processor.h"


DVBS2_FEC_Processor::DVBS2_FEC_Processor(int argc, char** argv)
{
	cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Called: ";
	for (int i = 0; i < argc; ++i)
		cout << argv[i] << " ";
	cout << "\n";

	codec_params_ldpc = std::unique_ptr<factory::Codec_LDPC>(new factory::Codec_LDPC);
	codec_params_bch = std::unique_ptr<factory::Codec_BCH>(new factory::Codec_BCH);
	std::vector<factory::Factory*> params_list_ldpc = { codec_params_ldpc.get() };
	std::vector<factory::Factory*> params_list_bch = { codec_params_bch.get() };

	char** argv_ldpc;
	char** argv_bch;
	int argc_ldpc;
	int argc_bch;

	Frame_Length = Normal;

	std::vector<std::string> argList(argv, argv + argc);

	// ------------------------------ Begin Handle Command Line Arguments ------------------------------
	cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Beginning processing command line arguments" << std::endl;
	vector<char*> pointer_vec_general_config(0);
	vector<char*> pointer_vec_ldpc(1);
	vector<char*> pointer_vec_bch(1);
	pointer_vec_ldpc[0] = (char*) argList[0].data();
	pointer_vec_bch[0] = (char*) argList[0].data();
	for(unsigned i = 1; i < argList.size(); i+=2)
	{
		int ldpc_index = argList[i].find("ldpc-");
		int bch_index = argList[i].find("bch-");
		int fec_processor_config_index = argList[i].find("fec-processor-config-mode");
		int esn0_db_index = argList[i].find("esn0_db");
		if (ldpc_index >= 0)
		{
			argList[i].replace(ldpc_index, 5, "");
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i].data());
			pointer_vec_ldpc.insert(pointer_vec_ldpc.end(), (char*) argList[i+1].data());
		}
		else if (bch_index >= 0)
		{
			argList[i].replace(bch_index, 4, "");
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i].data());
			pointer_vec_bch.insert(pointer_vec_bch.end(), (char*) argList[i+1].data());
		}
		else if (fec_processor_config_index >= 0)
		{
			pointer_vec_general_config.insert(pointer_vec_general_config.end(), (char*) argList[i].data());
			pointer_vec_general_config.insert(pointer_vec_general_config.end(), (char*) argList[i+1].data());
		}
		else if (esn0_db_index >= 0)
		{
			pointer_vec_general_config.insert(pointer_vec_general_config.end(), (char*) argList[i].data());
			pointer_vec_general_config.insert(pointer_vec_general_config.end(), (char*) argList[i+1].data());
		}
		else
		{
			/* code */
		}
	}

	// Handle general config arguments
	int index_fec_processor_mode = find_parameter(pointer_vec_general_config, "--fec-processor-config-mode");
	if (index_fec_processor_mode == -1)
		FEC_Mode = Both;
	else
	{
		if (strcmp(pointer_vec_general_config[index_fec_processor_mode+1], "LDPC")==0)
			FEC_Mode = LDPC;
		else if (strcmp(pointer_vec_general_config[index_fec_processor_mode+1], "BCH")==0)
			FEC_Mode = LDPC;
		else if (strcmp(pointer_vec_general_config[index_fec_processor_mode+1], "BOTH")==0)
			FEC_Mode = Both;
		else
			throw std::invalid_argument( "Invalid FEC mode (--fec-processor-config-mode) - must be LDPC, BCH, or BOTH" );
	}
	int index_esn0_db = find_parameter(pointer_vec_general_config, "--esn0_db");
	auto esn0 = 8.0;
	if (index_esn0_db >= 0)
	{
		esn0 = std::stod(pointer_vec_general_config[index_esn0_db+1]);
	}

	// Handle LDPC arguments
	if (FEC_Mode==Both || FEC_Mode==LDPC)
	{
		// --ldpc-enc-cw-size
		int index_ldpc_enc_cw_size = find_parameter(pointer_vec_ldpc, "--enc-cw-size");
		if (index_ldpc_enc_cw_size == -1)
			throw std::invalid_argument( "LDPC Codeword size not set (missing --ldpc-enc-cw-size)" );
		if (atoi(pointer_vec_ldpc[index_ldpc_enc_cw_size+1]) == 64800)
		{
			Frame_Length = Normal;
			N_LDPC = 64800;
			N_BCH_Not_Shortened = (int)(pow(2, 16)+0.5);
		}
		else if (atoi(pointer_vec_ldpc[index_ldpc_enc_cw_size+1]) == 16200)
		{
			Frame_Length = Short;
			N_LDPC = 16200;
			N_BCH_Not_Shortened = (int)(pow(2, 14)+0.5);
		}
		else
			throw std::invalid_argument( "LDPC Codeword size (missing --ldpc-enc-cw-size) must be 64800 (for Normal frames) of 16200 (for Short frames)" );
		
		// --ldpc-enc-info-bits
		int index_ldpc_enc_info_bits = find_parameter(pointer_vec_ldpc, "--enc-info-bits");
		if (index_ldpc_enc_info_bits == -1)
			throw std::invalid_argument( "LDPC information bits size not set (missing --ldpc-enc-info-bits)" );
		// TODO: Error checking
		N_BCH_K_LDPC = atoi(pointer_vec_ldpc[index_ldpc_enc_info_bits+1]);

		argv_ldpc = pointer_vec_ldpc.data();
		argc_ldpc = pointer_vec_ldpc.size();

		cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Processing LDPC arguments";
		tools::Command_parser cp_ldpc(argc_ldpc, argv_ldpc, params_list_ldpc, true);
		cout << "  Done" << std::endl;
		if (argc_ldpc > 1 & cp_ldpc.parsing_failed())
		{
			cp_ldpc.print_help();
			cp_ldpc.print_warnings();
			cp_ldpc.print_errors();
			std::exit(1);
		}
		cp_ldpc.print_warnings();
	}

	// Handle BCH arguments
	if (FEC_Mode==Both || FEC_Mode==BCH)
	{
		// --bch-enc-cw-size
		int index_bch_enc_cw_size = find_parameter(pointer_vec_bch, "--enc-cw-size");
		if (index_bch_enc_cw_size == -1)
			throw std::invalid_argument( "BCH codeword size not set (missing --bch-enc-cw-size)" );
		if ((FEC_Mode==Both) & (atoi(pointer_vec_bch[index_bch_enc_cw_size+1]) != N_BCH_K_LDPC))
			throw std::invalid_argument( "LDPC information bits size (--ldpc-enc-cw-size) must equal BCH codeword size" );
		else if (FEC_Mode==BCH)
			N_BCH_K_LDPC = atoi(pointer_vec_bch[index_bch_enc_cw_size+1]);
		

		// --bch-enc-info-bits
		int index_bch_enc_info_bits = find_parameter(pointer_vec_bch, "--enc-info-bits");
		if (index_bch_enc_info_bits == -1)
			throw std::invalid_argument( "BCH information bits size not set (missing --bch-enc-info-bits)" );
		// TODO: Error checking
		K_BCH = atoi(pointer_vec_bch[index_bch_enc_info_bits+1]);

		// --bch-dec-corr-pow
		int index_bch_corr_pow = find_parameter(pointer_vec_bch, "--dec-corr-pow");
		if (index_bch_corr_pow == -1)
			throw std::invalid_argument( "BCH t correction power not set (missing --bch-dec-corr-pow)" );
		// TODO: Error checking
		T_BCH = atoi(pointer_vec_bch[index_bch_corr_pow+1]);

		string bch_enc_info_bits_str = std::to_string(N_BCH_Not_Shortened - 1 - (N_BCH_K_LDPC - K_BCH));
		string bch_enc_cw_size_str = std::to_string(N_BCH_Not_Shortened - 1);
		pointer_vec_bch[index_bch_enc_cw_size+1] = (char *) bch_enc_cw_size_str.c_str();
		pointer_vec_bch[index_bch_enc_info_bits+1] = (char *) bch_enc_info_bits_str.c_str();

		argv_bch = pointer_vec_bch.data();
		argc_bch = pointer_vec_bch.size();

		cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Processing BCH arguments:  ";
		for (int i=0; i< argc_bch; i++) cout << argv_bch[i] << " "; cout << std::flush;
		tools::Command_parser cp_bch(argc_bch, argv_bch, params_list_bch, true);
		cout << "  Done" << std::endl;
		if (argc_bch > 1 & cp_bch.parsing_failed())
		{
			cp_bch.print_help();
			cp_bch.print_warnings();
			cp_bch.print_errors();
			std::exit(1);
		}
		cp_bch.print_warnings();
	}

	cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Done processing command line arguments" << std::endl;
	// ------------------------------- End Handle Command Line Arguments -------------------------------

	if (FEC_Mode==Both || FEC_Mode==LDPC)
	{
		cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Handling LDPC CODEC... ";
		codec_siho_ldpc = std::unique_ptr<module::Codec_SIHO  <>>(codec_params_ldpc->build());
		encoder_ldpc = codec_siho_ldpc->get_encoder().get();
		decoder_siho_ldpc = codec_siho_ldpc->get_decoder_siho().get();
		list.insert(list.end(), encoder_ldpc);
		list.insert(list.end(), decoder_siho_ldpc);
		cout << "Done" << std::endl;
	}

	if (FEC_Mode==Both || FEC_Mode==BCH)
	{
		cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Handling BCH CODEC... ";
		codec_hiho_bch = std::unique_ptr<module::Codec_HIHO  <>>(codec_params_bch->build());
		encoder_bch = codec_hiho_bch->get_encoder().get();
		decoder_hiho_bch = codec_hiho_bch->get_decoder_hiho().get();
		list.insert(list.end(), encoder_bch);
		list.insert(list.end(), decoder_hiho_bch);
		cout << "Done" << std::endl;
	}

	cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Handling misc set-up";
    noise = std::unique_ptr<tools::Sigma<>>(new tools::Sigma<>());

	set_esn0(esn0);

	// configuration of the module tasks
	for (auto& mod : list)
    {
		for (auto& tsk : mod->tasks)
		{
			tsk->set_autoalloc(true); // enable the automatic allocation of the data in the tasks
			tsk->set_autoexec(false); // disable the auto execution mode of the tasks
			tsk->set_debug(false); // disable the debug mode
			tsk->set_debug_limit(16); // display only the 16 first bits if the debug mode is enabled
			tsk->set_stats(true); // enable the statistics

			// enable the fast mode (= disable the useless verifs in the tasks) if there is no debug and stats modes
			if (!tsk->is_debug() && !tsk->is_stats())
				tsk->set_fast(true);
		}
    }
	cout << "Done" << std::endl;

	cout << "DVBS2_FEC_Processor::DVBS2_FEC_Processor - Done!" << std::endl;
}


int DVBS2_FEC_Processor::find_parameter(vector<char*> vec_str, const char* string_to_find)
{
	int index_enc_cw_size = -1;
	for (int i = 0; i < vec_str.size(); i++)
		if (strcmp(vec_str[i], string_to_find) == 0)
		{
			index_enc_cw_size = i;
			break;
		}
	return index_enc_cw_size;
}


void DVBS2_FEC_Processor::set_esn0(const float esn0_db)
{
	const auto sigma = tools::esn0_to_sigma(esn0_db);
	const auto ebn0 = tools::esn0_to_ebn0(esn0_db);
    noise->set_noise(sigma, ebn0, esn0_db);

	// update the sigma of the modem and the channel
	if (FEC_Mode==Both || FEC_Mode==LDPC)
	{
		codec_siho_ldpc->set_noise(*noise);
	}
}


bool DVBS2_FEC_Processor::encode_ldpc(std::vector<int, std::allocator<int> > &U_K, std::vector<int, std::allocator<int> >&X_N, const int frame_id)
{
	bool debug = false;
	if (debug==true)
	{
		cout << "Size U_K = " << U_K.size() << endl;
		for (int i=0; i<50; i++)
			cout << U_K[i] << " ";
		cout << "\n";
	}
	if (debug==true)
	{
		cout << "Size X_N = " << X_N.size() << endl;
		for (int i=0; i<50; i++)
			cout << X_N[i] << " ";
		cout << "\n";
	}

	try
	{
		encoder_ldpc->encode(U_K, X_N);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	if (debug==true)
	{
		for (int i=0; i<20; i++)
			cout << X_N[i] << " ";
		cout << "\n";
	}
    return (true);
}


bool DVBS2_FEC_Processor::decode_ldpc(std::vector<float, std::allocator<float> > &Y_N, std::vector<int, std::allocator<int> >&V_N, const int frame_id, const bool include_cw)
{
	bool debug = false;
	if (debug==true)
	{
		cout << "DVBS2_FEC_Processor::decode_ldpc: Y_N.size()=" << Y_N.size() << "  V_N.size()=" << V_N.size() << "  frame_id=" << frame_id << "  include_cw=" << include_cw << std::endl;
		cout<< "Y_N = ";
		for (int i=0; i<20; i++)
			cout << Y_N[i] << " ";
		cout << " ... ";
		for (int i=Y_N.size()-20; i<Y_N.size(); i++)
			cout << Y_N[i] << " ";
		cout << std::endl;
	}

	bool passed = false;
	try
	{
		std::vector<int> V_N_Temp;
		if (V_N.size() < N_LDPC)
		{
			V_N_Temp = std::vector<int>(N_LDPC);
		}
		else
		{
			V_N_Temp = V_N;
		}
		V_N_Temp = std::vector<int>(N_LDPC);

	    decoder_siho_ldpc->decode_siho_cw(Y_N, V_N_Temp);
		int* a = &V_N_Temp[0];
		passed = ((module::Decoder_LDPC_BP_flooding<int,float,tools::Update_rule_SPA<float>> *)decoder_siho_ldpc)->check_syndrome_hard(a);


		if (include_cw)
			V_N = V_N_Temp;
		else
			V_N = std::vector<int>(V_N_Temp.begin(), V_N_Temp.begin() + N_BCH_K_LDPC);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	//if (incude_cw)
	//	V_N = std::vector<int>(V_N_extended.begin(), V_N_extended.begin() + N_BCH_K_LDPC);
	//else
	//	V_N = std::vector<int>(V_N_extended.begin() + (N_BCH_K_LDPC-K_BCH), V_N_extended.begin() + N_BCH_K_LDPC);
	
	if (debug==true)
	{
		cout<< "V_N = ";
		for (int i=0; i<20; i++)
			cout << V_N[i] << " ";
		cout << " ... ";
		for (int i=V_N.size()-20; i<V_N.size(); i++)
			cout << V_N[i] << " ";
		cout << std::endl << std::endl;
	}
    return passed;
}

/**
 * @brief Performs BCH encoding of a BBFRAME
 * 
 * @param U_K BBFRAME bits
 * @param X_N BCH + BBFRAME Bits (puts BCH bits after BBFRAME bits, which differs from hwo AFF3CT does it by default)
 * @param frame_id (Optional) Frame ID of frame
 * @return true 
 *
 *  
 * @note Assumes data is structured as:  FIX THIS----------------------------------------------------------------------------------------------
 *   |--- BCH Bits ---|------------------- BBFRAME Bits -------------------|
 *   0            NBCH-KBCH                                              NBCH
 * 
 */
bool DVBS2_FEC_Processor::encode_bch(std::vector<int, std::allocator<int> > &U_K, std::vector<int, std::allocator<int> >&X_N, const int frame_id)
{
	bool debug = false;
	if (debug==true)
	{
		cout << "Size U_K = " << U_K.size() << endl;
		for (int i=0; i<50; i++)
			cout << U_K[i] << " ";
		cout << "\n";
	}
	if (debug==true)
	{
		cout << "Size X_N = " << X_N.size() << endl;
		for (int i=0; i<50; i++)
			cout << X_N[i] << " ";
		cout << "\n";
	}
	
	std::vector<int> U_K_extended = U_K;
	std::reverse(U_K_extended.begin(),U_K_extended.end()); 
	std::vector<int> X_N_extended = X_N;
	
	if (Frame_Length == Normal)
	{
		for (int i=U_K.size(); i < N_BCH_Not_Shortened-(16*T_BCH)-1; i++)
			U_K_extended.insert(U_K_extended.end(), 0);
		for (int i=X_N.size(); i < N_BCH_Not_Shortened-1; i++)
			X_N_extended.insert(X_N_extended.end(), 0);
	}
	else if (Frame_Length == Short)
	{
		for (int i=U_K.size(); i < N_BCH_Not_Shortened - 1 - (N_BCH_K_LDPC - K_BCH); i++)
			U_K_extended.insert(U_K_extended.end(), 0);
		for (int i=X_N.size(); i < N_BCH_Not_Shortened-1; i++)
			X_N_extended.insert(X_N_extended.end(), 0);
	}
	else
	{
		/* code */
	}
	

	try
	{
		encoder_bch->encode(U_K_extended, X_N_extended);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	// Reverse order of vector so that parity bits follow data bits
	for (int i = 0; i < N_BCH_K_LDPC; i++)
		X_N[i] = X_N_extended[N_BCH_K_LDPC-1-i];

	if (debug==true)
	{
		for (int i=0; i<20; i++)
			cout << X_N[i] << " ";
		cout << std::endl << std::endl;
	}
    return (true);
}


/**
 * @brief Performs BCD decoding of a FECFRAME
 * 
 * @param Y_N BCH parity bits and BBFRAME bits (Assumes BCH parity bits are at the beginning of the frame)
 * @param V_N Output buffer for decoded bits
 * @param frame_id (Optional) Frame ID of PL frame
 * @param incude_cw Specifieds whether entire codeword should be returned (bit 0 to bit NBCH) or just BBFRAME bits (bit NBCH-KBCH to bit NBCH)
 * @return true 
 * 
 * @note Assumes data is structured as:
 *   |--- BCH Bits ---|------------------- BBFRAME Bits -------------------|
 *   0            NBCH-KBCH                                              NBCH
 * 
 */
bool DVBS2_FEC_Processor::decode_bch(std::vector<int, std::allocator<int> > &Y_N, std::vector<int, std::allocator<int> >&V_N, const int frame_id, const bool include_cw)
{
	bool debug = false;
	if (debug==true)
	{
		cout << "DVBS2_FEC_Processor::decode_bch: Y_N.size()=" << Y_N.size() << "  V_N.size()=" << V_N.size() << "  frame_id=" << frame_id << "  include_cw=" << include_cw << std::endl;
		cout<< "Y_N = ";
		for (int i=0; i<20; i++)
			cout << Y_N[i] << " ";
		cout << " ... ";
		for (int i=Y_N.size()-20; i<Y_N.size(); i++)
			cout << Y_N[i] << " ";
		cout << std::endl;
	}

	std::vector<int> Y_N_extended = std::vector<int>(Y_N.size());
	for (int i = 0; i < N_BCH_K_LDPC; i++)
		Y_N_extended[i] = Y_N[i];
	std::vector<int> V_N_extended = V_N;
	
	if (Frame_Length == Normal)
	{
		for (int i=Y_N_extended.size(); i < N_BCH_Not_Shortened-1; i++)
		{
			Y_N_extended.insert(Y_N_extended.end(), 0);
		}
		for (int i=V_N_extended.size(); i < N_BCH_Not_Shortened-1; i++)
		{
			V_N_extended.insert(V_N_extended.end(), 0);
		}
	}
	else if (Frame_Length == Short)
	{
		for (int i=Y_N_extended.size(); i < N_BCH_Not_Shortened-1; i++)
		{
			Y_N_extended.insert(Y_N_extended.end(), 0);
		}
		for (int i=V_N_extended.size(); i < N_BCH_Not_Shortened-1; i++)
		{
			V_N_extended.insert(V_N_extended.end(), 0);
		}
	}
	else
	{

	}

	try
	{
		decoder_hiho_bch->decode_hiho_cw(Y_N_extended, V_N_extended);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	// Reverse order of vector so that parity bits follow data bits
	for (int i = 0; i < K_BCH; i++)
		V_N[i] = V_N_extended[i];
	if (include_cw)
		for (int i = K_BCH; i < N_BCH_K_LDPC; i++)
			V_N[i] = V_N_extended[i];
	/*
	for (int i = 0; i < K_BCH; i++)
		V_N[i] = V_N_extended[N_BCH_K_LDPC-1-i];
	if (include_cw)
		for (int i = K_BCH; i < N_BCH_K_LDPC; i++)
			V_N[i] = V_N_extended[N_BCH_K_LDPC-1-i];
	*/
	if (debug==true)
	{
		cout<< "V_N = ";
		for (int i=0; i<20; i++)
			cout << V_N[i] << " ";
		cout << " ... ";
		for (int i=V_N.size()-20; i<V_N.size(); i++)
			cout << V_N[i] << " ";
		cout << std::endl << std::endl;
	}
    return (true);
}

bool DVBS2_FEC_Processor::decode_ldpc_bch(std::vector<float, std::allocator<float> > &Y_N, std::vector<int, std::allocator<int> >& V_N, const int frame_id)
{
	bool debug = false;
	if (debug==true)
	{
		cout << "DVBS2_FEC_Processor::decode_ldpc_bch: Y_N.size()=" << Y_N.size() << "  V_N.size()=" << V_N.size() << "  frame_id=" << frame_id << std::endl;
		cout<< "Y_N = ";
		for (int i=0; i<20; i++)
			cout << Y_N[i] << " ";
		cout << " ... ";
		for (int i=Y_N.size()-20; i<20; i++)
			cout << Y_N[i] << " ";
		cout << std::endl << std::endl;
	}

	std::vector<int> V_N_LDPC = std::vector<int>(N_BCH_K_LDPC);
	bool ldpc_success = decode_ldpc(Y_N, V_N_LDPC, frame_id, false);
	//V_N_LDPC = std::vector<int>(V_N_LDPC.begin(), V_N_LDPC.begin() + N_BCH_K_LDPC);
	decode_bch(V_N_LDPC, V_N, -1, false);
	return(ldpc_success);
}


void DVBS2_FEC_Processor::Reset()
{
	decoder_hiho_bch->reset();
	decoder_siho_ldpc->reset();
}


void DVBS2_FEC_Processor::DeleteObjects()
{
	delete encoder_ldpc;
	delete encoder_bch;
	delete decoder_siho_ldpc;
	delete decoder_siso_ldpc;
	delete decoder_hiho_bch;
	list.clear();
}


extern "C" {
    DVBS2_FEC_Processor* DVBS2_FEC_Processor_Init(int argc, char** argv)
	{
		return new DVBS2_FEC_Processor(argc, argv);
	}

	void DVBS2_FEC_Processor_Reset(DVBS2_FEC_Processor* Processor)
	{
		Processor->Reset();
		return;
	}

	void DVBS2_FEC_Processor_DeleteObjects(DVBS2_FEC_Processor* Processor)
	{
		Processor->DeleteObjects();
		return;
	}

	void DVBS2_FEC_Processor_Set_EsN0(DVBS2_FEC_Processor* Processor, float esn0_db)
	{
		Processor->set_esn0(esn0_db);
		return;
	}

	bool DVBS2_FEC_Processor_Encode_LDPC(DVBS2_FEC_Processor* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id)
	{
		//cout << "In DVBS2_FEC_Processor_Encode_LDPC" << endl;
		std::vector<int> U_K_vector(U_K, U_K+Num_U_K);
		std::vector<int> X_N_vector(X_N, X_N+Num_X_N);
		bool result = Processor->encode_ldpc(U_K_vector, X_N_vector, frame_id);
		for (int i = 0; i < Num_X_N; i++)
			X_N[i] = X_N_vector[i];
		//for (int i=0; i<20; i++)
		//	cout << X_N[i] << " ";
		//cout << "\n";
		return result;
	}

	bool DVBS2_FEC_Processor_Decode_LDPC(DVBS2_FEC_Processor* Processor, float Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw)
	{
		//cout << "In DVBS2_FEC_Processor_Decode_LDPC" << endl;
		std::vector<float> Y_N_vector(Y_N, Y_N+Num_Y_N);
		std::vector<int> V_N_vector(V_N, V_N+Num_V_N);
		bool result = Processor->decode_ldpc(Y_N_vector, V_N_vector, frame_id, incude_cw);
		for (int i = 0; i < Num_V_N; i++)
			V_N[i] = V_N_vector[i];
		//for (int i=0; i<20; i++)
		//	cout << V_N[i] << " ";
		//cout << "\n";
		return result;
	}

	bool DVBS2_FEC_Processor_Encode_BCH(DVBS2_FEC_Processor* Processor, int U_K[], int Num_U_K, int X_N[], int Num_X_N, const int frame_id)
	{
		//cout << "In DVBS2_FEC_Processor_Encode_BCH" << endl;
		std::vector<int> U_K_vector(U_K, U_K+Num_U_K);
		std::vector<int> X_N_vector(X_N, X_N+Num_X_N);
		bool result = Processor->encode_bch(U_K_vector, X_N_vector, frame_id);
		for (int i = 0; i < Num_X_N; i++)
			X_N[i] = X_N_vector[i];
		//for (int i=0; i<20; i++)
		//	cout << X_N[i] << " ";
		//cout << "\n";
		return result;
	}

	bool DVBS2_FEC_Processor_Decode_BCH(DVBS2_FEC_Processor* Processor, int Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id, const bool incude_cw)
	{
		//cout << "In DVBS2_FEC_Processor_Decode_BCH" << endl;
		std::vector<int> Y_N_vector(Y_N, Y_N+Num_Y_N);
		std::vector<int> V_N_vector(V_N, V_N+Num_V_N);
		bool result = Processor->decode_bch(Y_N_vector, V_N_vector, frame_id, incude_cw);
		for (int i = 0; i < Num_V_N; i++)
			V_N[i] = V_N_vector[i];
		//for (int i=0; i<20; i++)
		//	cout << V_N[i] << " ";
		//cout << "\n";
		return result;
	}

	bool DVBS2_FEC_Processor_Decode_LDPC_BCH(DVBS2_FEC_Processor* Processor, float Y_N[], int Num_Y_N, int V_N[], int Num_V_N, const int frame_id)
	{
		std::vector<float> Y_N_vector(Y_N, Y_N+Num_Y_N);
		std::vector<int> V_N_vector(V_N, V_N+Num_V_N);
		bool result = Processor->decode_ldpc_bch(Y_N_vector, V_N_vector, frame_id);
		for (int i = 0; i < Num_V_N; i++)
			V_N[i] = V_N_vector[i];
		//for (int i=0; i<20; i++)
		//	cout << V_N[i] << " ";
		//cout << "\n";
		return (result);
	}
}
