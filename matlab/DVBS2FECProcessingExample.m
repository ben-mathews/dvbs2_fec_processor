subsystemType = '8PSK 9/10';
EsNodB        = 20;
t_bch         = 8
numFrames     = 1;

%
% Run MATLAB functions to generate reference vectors
%
configureDVBS2Demo
databits = randi(2,dvb.BCHMessageLength, 1)-1;
encbch = comm.BCHEncoder('CodewordLength', dvb.BCHCodewordLength, ...
	'MessageLength', dvb.BCHMessageLength, ...
	'PrimitivePolynomialSource', 'Property', ...
	'PrimitivePolynomial', dvb.BCHPrimitivePoly, ...
	'GeneratorPolynomialSource', 'Property', ...
	'GeneratorPolynomial', dvb.BCHGeneratorPoly, ...
	'CheckGeneratorPolynomial', false);

encldpc = comm.LDPCEncoder(dvb.LDPCParityCheckMatrix);

decldpc = comm.LDPCDecoder(dvb.LDPCParityCheckMatrix, ...
	'IterationTerminationCondition', 'Parity check satisfied', ...
	'MaximumIterationCount',         dvb.LDPCNumIterations, ...
	'NumIterationsOutputPort',       true);

bchEncOut = encbch(databits);
ldpcEncOut = encldpc(bchEncOut);
intrlvrOut = intrlvr(ldpcEncOut);

if dvb.ModulationOrder == 4 || dvb.ModulationOrder == 8
	modOut = pskModulator(intrlvrOut);
else
	modOut = dvbsapskmod(intrlvrOut, dvb.ModulationOrder, 's2', ...
		dvb.CodeRate, 'InputType', 'bit', 'UnitAveragePower', true);
end

chanOut = chan(modOut);

if dvb.ModulationOrder == 4 || dvb.ModulationOrder == 8
	demodOut = pskDemodulator(chanOut);
else
	demodOut = dvbsapskdemod(chanOut, dvb.ModulationOrder, 's2', ...
		dvb.CodeRate, 'OutputType', 'approxllr', 'NoiseVar', ...
		dvb.NoiseVar, 'UnitAveragePower', true);
end

deintrlvrOut = deintrlvr(demodOut);
[ldpcDecOut, numIter] = decldpc(deintrlvrOut);
bchDecOut = decbch(ldpcDecOut);
bbFrameRx = bchDecOut(1:dvb.NumInfoBitsPerCodeword,1);

%
% Use the MATLAB class that wraps our C/C++ code
%
nldpc = length(ldpcEncOut);
kldpc = length(bchEncOut);
kbch = length(databits);

DVBS2FECProcessingObj = DVBS2FECProcessing(nldpc, kldpc, kbch, t_bch);

bchEncOut2 = DVBS2FECProcessingObj.Encode_BCH(databits);
if sum(int32(bchEncOut)~=bchEncOut2) > 0
	warning('Difference in Encoded BCH bits');
end

ldpcEncOut2 = DVBS2FECProcessingObj.Encode_LDPC(bchEncOut2);
if sum(int32(ldpcEncOut)~=ldpcEncOut2) > 0
	warning('Difference in Encoded LDPC bits');
end

ldpcDecOut2 = DVBS2FECProcessingObj.Decode_LDPC(deintrlvrOut);
if sum(int32(ldpcDecOut)~=ldpcDecOut2(1:length(ldpcDecOut))) > 0
	warning('Difference in Decoded LDPC bits');
end

databits2 = DVBS2FECProcessingObj.Decode_BCH(bchEncOut2);
if sum(int32(databits)~=databits2(1:length(databits))) > 0
	warning('Difference in Decoded LDPC bits');
end

delete(DVBS2FECProcessingObj)