% export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.28
classdef DVBS2FECProcessing
    properties (SetAccess = private)
        libdvbs2_fec_processor_obj
    end
    
    properties (SetAccess = public)
        n_ldpc = 0
        k_ldpc = 0
        k_bch = 0
        t_bch = 0
    end
   
    methods
        function obj = DVBS2FECProcessing(n_ldpc, k_ldpc, k_bch, t_bch)
            obj.n_ldpc = int32(n_ldpc);
            obj.k_ldpc = int32(k_ldpc);
            obj.k_bch = int32(k_bch);
            obj.t_bch = int32(t_bch);
            if libisloaded('libdvbs20x2Dfec0x2Dprocessor')
                unloadlibrary('libdvbs20x2Dfec0x2Dprocessor')
                pause(0.25); % Seems to reduce crashiness
            end
            warning('off', 'MATLAB:loadlibrary:ClassRenamed')
            loadlibrary('/home/ben/dev/gitlab/dvbs2_fec_processor/build/src/dvbs2-fec-processor/lib/libdvbs2-fec-processor.so', ...
                        '/home/ben/dev/gitlab/dvbs2_fec_processor/src/dvbs2-fec-processor/src/DVBS2_FEC_Processor_Matlab.h');
            %libisloaded('libdvbs20x2Dfec0x2Dprocessor')
            argv = {'dummy', '--ldpc-enc-info-bits', num2str(k_ldpc), '--ldpc-enc-cw-size', num2str(n_ldpc), '--ldpc-enc-type', 'LDPC_DVBS2', '--bch-enc-info-bits',  num2str(k_bch), '--bch-enc-cw-size', num2str(k_ldpc), '--bch-dec-corr-pow', num2str(t_bch)};
            argc = length(argv);
            obj.libdvbs2_fec_processor_obj = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Init', argc, argv);
            pause(0.25); % Seems to reduce crashiness
        end
        
        
        function delete(obj)
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_DeleteObjects', obj.libdvbs2_fec_processor_obj);
            if libisloaded('libdvbs20x2Dfec0x2Dprocessor')
                unloadlibrary('libdvbs20x2Dfec0x2Dprocessor')
                pause(0.25); % Seems to reduce crashiness
            end
        end
        
        
        function X_N = Encode_LDPC(obj, U_K, FrameId)
            if nargin<3; FrameId = 0; end
            U_K = int32(U_K);
            Num_U_K = int32(length(U_K));
            Num_X_N = obj.n_ldpc;
            X_N = int32(zeros(Num_X_N, 1));
            X_N_Ptr = libpointer('int32Ptr',X_N);
            FrameId = int32(FrameId);
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Encode_LDPC', obj.libdvbs2_fec_processor_obj, U_K, Num_U_K, X_N_Ptr, Num_X_N, FrameId);
            X_N = X_N_Ptr.Value;
        end
        
        
        function V_N = Decode_LDPC(obj, Y_N, FrameId)
            if nargin<3; FrameId = 0; end
            Y_N = single(Y_N);
            Num_Y_N = int32(length(Y_N));
            V_N = int32(zeros(Num_Y_N, 1));
            V_N_Ptr = libpointer('int32Ptr',V_N);
            Num_V_N = int32(length(V_N));
            FrameId = int32(FrameId);
            IncludeCW = boolean(false);
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Decode_LDPC', obj.libdvbs2_fec_processor_obj, Y_N, Num_Y_N, V_N_Ptr, Num_V_N, FrameId, IncludeCW);
            V_N = V_N_Ptr.Value;
        end
        
        
        function X_N = Encode_BCH(obj, U_K, FrameId)
            if nargin<3; FrameId = 0; end
            U_K = int32(U_K);
            Num_U_K = int32(length(U_K));
            Num_X_N = obj.k_ldpc;
            X_N = int32(zeros(Num_X_N, 1));
            X_N_Ptr = libpointer('int32Ptr',X_N);
            FrameId = int32(FrameId);
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Encode_BCH', obj.libdvbs2_fec_processor_obj, U_K, Num_U_K, X_N_Ptr, Num_X_N, FrameId);
            X_N = X_N_Ptr.Value;
        end
        
        
        function V_N = Decode_BCH(obj, Y_N, FrameId)
            if nargin<3; FrameId = 0; end
            Y_N = int32(Y_N);
            Num_Y_N = int32(length(Y_N));
            V_N = int32(zeros(Num_Y_N, 1));
            V_N_Ptr = libpointer('int32Ptr',V_N);
            Num_V_N = int32(length(V_N));
            FrameId = int32(FrameId);
            IncludeCW = boolean(false);
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Decode_BCH', obj.libdvbs2_fec_processor_obj, Y_N, Num_Y_N, V_N_Ptr, Num_V_N, FrameId, IncludeCW);
            V_N = V_N_Ptr.Value;
        end
        
        
        function V_N = Decode_LDPC_BCH(obj, Y_N, FrameId)
            if nargin<3; FrameId = 0; end
            Y_N = single(Y_N);
            Num_Y_N = int32(length(Y_N));
            V_N = int32(zeros(obj.k_bch, 1));
            V_N_Ptr = libpointer('int32Ptr',V_N);
            Num_V_N = int32(obj.k_bch);
            FrameId = int32(FrameId);
            result = calllib('libdvbs20x2Dfec0x2Dprocessor','DVBS2_FEC_Processor_Decode_LDPC_BCH', obj.libdvbs2_fec_processor_obj, Y_N, Num_Y_N, V_N_Ptr, Num_V_N, FrameId);
            V_N = V_N_Ptr.Value;
        end
    end
end
