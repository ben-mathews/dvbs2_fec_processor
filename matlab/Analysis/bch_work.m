
% References:
% 1) https://comblock.com/download/com1209Asoft.pdf
% 2) https://pdfs.semanticscholar.org/17f7/e05c51de0633e9c391fc54f5c3c93bcbc21f.pdf

ModCod_EsN0 = {'8PSK 9/10',   2.679207, 10.98, 8}
subsystemType = ModCod_EsN0{1};  % Constellation and LDPC code rate
EsNodB = ModCod_EsN0{3} + 10;     % Energy per symbol to noise PSD ratio in dB
t_bch = ModCod_EsN0{4};
numFrames     = 1;             % Number of frames to simulate

% Initialize
configureDVBS2Demo

databits = randi(2,dvb.BCHMessageLength, 1)-1;

encbch = comm.BCHEncoder('CodewordLength', dvb.BCHCodewordLength, ...
    'MessageLength', dvb.BCHMessageLength, ...
    'PrimitivePolynomialSource', 'Property', ...
    'PrimitivePolynomial', dvb.BCHPrimitivePoly, ...
    'GeneratorPolynomialSource', 'Property', ...
    'GeneratorPolynomial', dvb.BCHGeneratorPoly, ...
    'CheckGeneratorPolynomial', false);

encldpc = comm.LDPCEncoder(dvb.LDPCParityCheckMatrix);

decldpc = comm.LDPCDecoder(dvb.LDPCParityCheckMatrix, ...
    'IterationTerminationCondition', 'Parity check satisfied', ...
    'MaximumIterationCount',         dvb.LDPCNumIterations, ...
    'NumIterationsOutputPort',       true);

bchEncOut = encbch(databits);

ldpcEncOut = encldpc(bchEncOut);

intrlvrOut = intrlvr(ldpcEncOut);

if dvb.ModulationOrder == 4 || dvb.ModulationOrder == 8
    modOut = pskModulator(intrlvrOut);
else
    modOut = dvbsapskmod(intrlvrOut, dvb.ModulationOrder, 's2', ...
        dvb.CodeRate, 'InputType', 'bit', 'UnitAveragePower', true);
end

chanOut = chan(modOut);

if dvb.ModulationOrder == 4 || dvb.ModulationOrder == 8
    demodOut = pskDemodulator(chanOut);
else
    demodOut = dvbsapskdemod(chanOut, dvb.ModulationOrder, 's2', ...
        dvb.CodeRate, 'OutputType', 'approxllr', 'NoiseVar', ...
        dvb.NoiseVar, 'UnitAveragePower', true);
end

deintrlvrOut = deintrlvr(demodOut);
[ldpcDecOut, numIter] = decldpc(deintrlvrOut);
bchDecOut = decbch(ldpcDecOut);
bbFrameRx = bchDecOut(1:dvb.NumInfoBitsPerCodeword,1);



%%%%%%% Begin Encode %%%%%%%

x = zeros(encbch.CodewordLength-encbch.MessageLength+1, 1);
x(end) = 1;

m = gfconv(databits(end:-1:1)', x', 2);
m = [m, zeros(1, encbch.MessageLength-length(m))];
[q, r] = gfdeconv(m, dvb.BCHGeneratorPoly(end:-1:1), 2);
r = [zeros(encbch.CodewordLength-encbch.MessageLength-length(r)); r(end:-1:1)'];

bchEncOut2 = [databits; r];
sum(bchEncOut ~= bchEncOut2)
sum(bchEncOut == bchEncOut2)

%%%%%%%% End Encode %%%%%%%%






%%%%%%% Begin Encode 2 %%%%%%%

x = zeros(encbch.CodewordLength-encbch.MessageLength+1, 1);
x(end) = 1;

K_BCH_extended = 2^16 - (encbch.CodewordLength-encbch.MessageLength);
databits_extended = zeros(1, K_BCH_extended);
databits_extended(1:length(databits)) = databits(end:-1:1);

m = gfconv(databits_extended, x', 2);
m = [m, zeros(1, K_BCH_extended-length(m))];
[q, r] = gfdeconv(m, dvb.BCHGeneratorPoly(end:-1:1), 2);

bch_bits = bchEncOut(end:-1:(length(databits)+1))'

r = [zeros(encbch.CodewordLength-encbch.MessageLength-length(r)); r(end:-1:1)'];

bch_bits = bchEncOut((length(databits)+1):end)'

bchEncOut2 = [databits; r];
sum(bchEncOut ~= bchEncOut2)
sum(bchEncOut == bchEncOut2)

%%%%%%%% End Encode 2 %%%%%%%%





b = [0 1 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1];
b = [1 0 1 1 0 1 0 0 0 0 0 0 0 0 0 0 1];
b = [1 1 0 0 1 1 1 0 1 0 0 0 0 0 0 0 1];
[q1, r1] = gfdeconv(bchEncOut', b, 2);
[q2, r2] = gfdeconv(bchEncOut(end:-1:1)', b, 2);

roots(gf(b, 16, primpoly(16, 'min')))


prim_poly16 = primpoly(16, 'min'); 
alpha = gf(2,16,prim_poly16);

[q, r] = gfdeconv(databits, encbch.GeneratorPolynomial)
sum(databits' ~= bchEncOut(1:length(databits))')
bchDecOut_BCH_bits = bchEncOut(length(databits)+1:end)'

encbch.GeneratorPolynomial