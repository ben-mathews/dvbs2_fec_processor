============
Py DVBS2 FEC
============


.. image:: https://img.shields.io/pypi/v/py_dvbs2_fec.svg
        :target: https://gitlab.com/ben-mathews/dvbs2_fec_processor/issues

.. image:: https://img.shields.io/travis/ben-mathews/py_dvbs2_fec.svg
        :target: https://travis-ci.org/ben-mathews/py_dvbs2_fec

.. image:: https://readthedocs.org/projects/py-dvbs2-fec/badge/?version=latest
        :target: https://py-dvbs2-fec.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




DVB-S2 FEC processing


* Free software: MIT license
* Documentation: https://py-dvbs2-fec.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
