# -*- coding: utf-8 -*-

"""Top-level package for Py DVBS2 FEC."""

__author__ = """Ben Mathews"""
__email__ = 'beniam@yahoo.com'
__version__ = '0.1.0'

from py_dvbs2_fec.py_dvbs2_fec import DVBS2_FEC_Processor, DVBS2_FEC_Processor_Bank