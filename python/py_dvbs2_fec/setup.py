#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [ ]

setup_requirements = [ ]

test_requirements = [ ]

setup(
    author="Ben Mathews",
    author_email='beniam@yahoo.com',
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="DVB-S2 FEC processing",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='py_dvbs2_fec',
    name='py_dvbs2_fec',
    packages=find_packages(include=['py_dvbs2_fec', 'py_dvbs2_fec.py_dvbs2_fec']),
    package_data={
        'py_dvbs2_fec': ['lib/libaff3ct-2.3.5-76-g781fbf7.so'],
        'py_dvbs2_fec  ': ['lib/libdvbs2-fec-processor.so'],
    },
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/ben-mathews/dvbs2_fec_processor/python/py_dvbs2_fec',
    version='0.1.0',
    zip_safe=False,
)
