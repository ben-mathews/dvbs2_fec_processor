.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Py DVBS2 FEC, run this command in your terminal:

.. code-block:: console

    $ pip install py_dvbs2_fec

This is the preferred method to install Py DVBS2 FEC, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Py DVBS2 FEC can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git@gitlab.com:ben-mathews/dvbs2_fec_processor.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/ben-mathews/py_dvbs2_fec/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. GitLab repo: https://gitlab.com/ben-mathews/dvbs2_fec_processor
.. _tarball: https://gitlab.com/ben-mathews/dvbs2_fec_processor/tarball/master
